import type { NextPage } from 'next'
import Head from 'next/head'
import BillPage from '../components/BillPage/BillPage'
import styles from '../styles/Home.module.scss'

const Bill: NextPage = () => {
  
  return (
    <div className={styles.home_container}>
        <Head>
          <title>Minimal Store</title>
          <meta name="description" content="Generated by create next app" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <BillPage/>
    </div>
  )
}

export default Bill