import {createSlice} from "@reduxjs/toolkit"

const initialState = {
    items: []
};

export const basketSlice = createSlice({
    name: "basket",
    initialState,
    reducers: {
        addToBasket: (state, action) => {
            const itemIndex = state.items.findIndex(
                (item)=> item.id === action.payload.id
            )
            if(itemIndex >= 0){
                state.items[itemIndex].newQuantity += 1
            } else {
            const tempItem ={...action.payload}
            state.items.push(tempItem)
            }
            // state.items = [...state.items, action.payload]
        },
        removeFromBasket: (state, action) => {
            const index = state.items.findIndex(
                basketItem => basketItem.id === action.payload.id
            );

            let newBasket = [...state.items];

            if (index >= 0) {
                newBasket.splice(index, 1)
            }else {
                console.warn('can')
            }
            state.items = newBasket;
        },
        resetBasket: (state) => {
            state.items = []
        }
    }
});


export const {addToBasket, removeFromBasket, resetBasket} = basketSlice.actions;

export const selectItems = (state) => state.basket.items;
export const selectTotal = (state) => state.basket.items.reduce((total, item) => total + (item.price * item.newQuantity), 0)
export default basketSlice.reducer;