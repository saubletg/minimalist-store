import {createSlice} from "@reduxjs/toolkit"

const initialState = {
    values: []
};

export const formSlice = createSlice({
    name: "form",
    initialState,
    reducers: {
        addToForm: (state, action) => {
            state.values = [...state.values, action.payload]
        },

        resetForm: (state) => {
            state.values = []
        }
    }
});

export const {addToForm, resetForm} = formSlice.actions;

export const selectValues = (state) => state.form.values;
export default formSlice.reducer;

