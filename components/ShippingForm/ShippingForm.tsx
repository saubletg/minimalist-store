import React from 'react'
import styles from './ShippingForm.module.scss'

function ShippingForm({handleShippingChange, data}:any) {
    
  return (
    <div className={styles.checkout_body_step_wrapper}>
        <div className={styles.checkout_step_title_wrapper}>
            <h2 className={styles.checkout_step_title}>
                2.Shipping
            </h2>
        </div>
        <div className={styles.checkout_step_form_container}>
            <form action="" className={styles.shipping_form}>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="email" className={styles.shipping_label}>Email .</label>
                    <input type="email" className={styles.shipping_input} name="email" 
                    onChange={handleShippingChange} value={data.email}/>
                </fieldset>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="name" className={styles.shipping_label}>Fullname .</label>
                    <input type="text" className={styles.shipping_input} name="fullName" 
                    onChange={handleShippingChange} value={data.fullName}/>
                </fieldset>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="address" className={styles.shipping_label}>Street Address .</label>
                    <input type="text" className={styles.shipping_input} name="streetAddress1" 
                    onChange={handleShippingChange} value={data.streetAddress1}/>
                    <input type="text" className={styles.shipping_input} name="streetAddress2" 
                    onChange={handleShippingChange} value={data.streetAddress2}/>
                </fieldset>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="city" className={styles.shipping_label}>City .</label>
                    <input type="text" className={styles.shipping_input} name="city" 
                    onChange={handleShippingChange} value={data.city}/>
                </fieldset>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="state" className={styles.shipping_label}>State/Province .</label>
                    <input type="text" className={styles.shipping_input} name="state" 
                    onChange={handleShippingChange} value={data.state}/>
                </fieldset>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="zip" className={styles.shipping_label}>Zip/Postal Code .</label>
                    <input type="text" className={styles.shipping_input} name="zip" 
                    onChange={handleShippingChange} value={data.zip}/>
                </fieldset>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="country" className={styles.shipping_label}>Country .</label>
                    <input type="text" className={styles.shipping_input} name="country" 
                    onChange={handleShippingChange} value={data.country}/>
                </fieldset>
            </form>
        </div>
    </div>
  )
}

export default ShippingForm