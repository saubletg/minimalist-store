import React, { useState } from 'react'
import styles from './Item.module.scss'
import Image from 'next/image'
import Link from 'next/link'
import { useDispatch } from 'react-redux';
import {addToBasket} from '../../slices/basketSlice'
import { ItemType} from '../../typings';

function Item({id, badge, price, image, alt, description, name, quantity}:ItemType) {

    const [newQuantity, setNewQuantity] = useState(quantity);

    const handleQuantity = (e:any) => {
        setNewQuantity(parseInt(e.target.value))
    }

    const dispatch = useDispatch();
    const addItemToBasket = () => {
        const product = {
            id, 
            badge, 
            price,
            image, 
            alt, 
            description, 
            name,
            newQuantity
        };
        
        dispatch(addToBasket(product))
    }
  return ( 
    <div className={styles.item_container}>
        <div className={styles.image_wrapper}>
            <Image
            src={`/${image}`}
            alt={alt}
            className={styles.image}
            height="300"
            width="300"
            objectFit= 'contain'/>
        </div>
        <div className={styles.item_wrapper}>
            <h3 className={styles.item_title}>{name}</h3>
            <div className={styles.item_price_wrapper}>
                $ {price} USD
            </div>
            {badge && <div className={styles.item_badge_wrapper}>
                <div className={styles.item_badge}>
                    free shipping
                </div>
            </div>}
            <div className={styles.item_image_wrapper}>
                <Image
                src={`/${image}`}
                alt={alt}
                className={styles.image}
                height="200"
                width="200"
                objectFit= 'contain'
                />
            </div>
            <div className={styles.image_description}>{description}</div>
            <div className={styles.item_cart_container}>
                <div className={styles.item_cart_wrapper}>
                    <input type="number" min="1" max="10" value={newQuantity} onChange={handleQuantity} className={styles.item_quantity}/>
                    <button className={styles.item_add} onClick={addItemToBasket}>Add to cart</button>
                </div>
                <Link href="/checkout" >
                    <a className={styles.item_buy} onClick={addItemToBasket}>Buy Now</a>
                </Link>
            </div>
        </div>
    </div>
  )
}


export default Item