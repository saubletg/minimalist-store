import Link from 'next/link'
import React, { useRef } from 'react'
import styles from './Hero.module.scss'
import {AiOutlineArrowDown} from 'react-icons/ai'


function Hero({itemsRef}:any) {
    
    const scrollToItems = () => itemsRef.current.scrollIntoView();

  return (
    <div className={styles.hero_container}>
        <div className={styles.hero_wrapper}>
            <aside className={styles.aside_wrapper}>
                <div className={styles.dot}></div>
                <div className={styles.text}>Hello</div>
                <div className={styles.dot}></div>
            </aside>
            <div className={styles.hero_paragraph_wrapper}>
                <h1 className={styles.hero_h1}>
                    Minimalist
                    <br/>
                    Store
                </h1>
                <button className={styles.hero_button} onClick={scrollToItems}>
                    See Items
                </button>
            </div>
        </div>
        <button className={styles.explore_wrapper} onClick={scrollToItems}>
            <p >Explore</p>
            <AiOutlineArrowDown/>
        </button> 
    </div>
  )
}

export default Hero