import Link from 'next/link'
import React from 'react'
import {IoMdClose} from 'react-icons/io'
import styles from './Menu.module.scss'

interface functionProps{
    handleMenu(): void
}
const Menu : React.FC <functionProps>= ({handleMenu}) => {
  return (
    <div className={styles.menu_wrapper}>
        <div className={styles.icon_wrapper} onClick={handleMenu}>
            <IoMdClose/>
        </div>
        <h2 className={styles.menu_title}>
            Minimalist Store
        </h2>
        <nav className={styles.menu_nav}>
            <Link href="/">
                <a href="">
                    Home
                </a>
            </Link>
            <Link href="/">
                <a href="">
                    Contact
                </a>
            </Link>
        </nav>
    </div>
  )
}

export default Menu