import Link from 'next/link'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectItems, selectTotal } from '../../slices/basketSlice'
import {addToForm} from '../../slices/formSlice'
import { CartItemType } from '../../typings'
import styles from './OrderSummary.module.scss'

function OrderSummary({shippingData, cardData}:any) {
    const items = useSelector(selectItems)
    const total = useSelector(selectTotal)
    
    const dispatch = useDispatch();
    const addValuesToForm = () => {
        const values = [
            shippingData,
            cardData
        ];
        
        dispatch(addToForm(values))
        console.log(values)
    }
    
  return (
    <div className={styles.order_container}>
        <div className={styles.checkout_body_step_wrapper}>
            <div className={styles.checkout_step_title_wrapper}>
                <h2 className={styles.checkout_step_title}>
                    Order Summary
                </h2>
            </div>
            <div className={styles.checkout_recap_wrapper}>
                {items.map(({id, price, name, newQuantity}:CartItemType)=>
                <div className={styles.checkout_item_wrapper} key={id}>
                    <div className={styles.checkout_recap_name}>
                        .{name}
                    </div>
                    <div className={styles.checkout_recap_price}>
                        $ {price} USD
                    </div>
                    <div className={styles.checkout_recap_quantity}>
                        x{newQuantity}
                    </div>
                </div>)
            }
            </div>
            <div className={styles.checkout_total_wrapper}>
                <div className={styles.checkout_total}>
                    Total
                </div>
                <div className={styles.checkout_total_price}>
                    $ {total} USD
                </div>
            </div>
        </div>
        <div className={styles.order_button_wrapper}>
            <Link href="/bill">
                <a className={styles.order_button} onClick={addValuesToForm}>
                    Place Order
                </a>
            </Link>
        </div>
        
    </div>
  )
}

export default OrderSummary