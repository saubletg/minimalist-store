import Link from 'next/link'
import React, { useRef } from 'react'
import styles from './CheckOutHero.module.scss'
import {AiOutlineArrowDown} from 'react-icons/ai'

function CheckOutHero() {

  return (
    <div className={styles.checkout_hero_container}>
        <div className={styles.checkout_hero_step_wrapper}>
            <div className={styles.checkout_hero_step_link}>
                <div className={styles.checkout_hero_step_text}>
                    1. summary
                </div>
            </div>
            <div className={styles.checkout_hero_step_line_wrapper}>
                <span className={styles.checkout_hero_step_line}></span>
            </div>
            <div className={styles.checkout_hero_step_link}>
                <div className={styles.checkout_hero_step_text}>
                    2. shipping
                </div>
            </div>
            <div className={styles.checkout_hero_step_line_wrapper}>
                <span className={styles.checkout_hero_step_line}></span>
            </div>
            <div className={styles.checkout_hero_step_link}>
                <div className={styles.checkout_hero_step_text}>
                    3. payment
                </div>
            </div>
            <div className={styles.checkout_hero_step_line}>
            </div>
        </div>
        <div className={styles.checkout_title_wrapper}>
            <h1 className={styles.checkout_title}>Your Order</h1>
            <div className={styles.checkout_title_line_wrapper}>
                <div className={styles.checkout_title_line}></div>
            </div>
        </div>
    </div>
  )
}

export default CheckOutHero