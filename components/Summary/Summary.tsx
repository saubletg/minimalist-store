import React from 'react'
import { useSelector } from 'react-redux'
import { selectItems } from '../../slices/basketSlice'
import { CartItemType } from '../../typings'
import ItemCheckOut from '../ItemCheckOut/ItemCheckOut'
import styles from './Summary.module.scss'

function Summary() {
  const items = useSelector(selectItems)

  return (
    <div className={styles.checkout_body_step_wrapper}>
        <div className={styles.checkout_step_title_wrapper}>
            <h2 className={styles.checkout_step_title}>
                1.Summary
            </h2>
        </div>
        <div className={styles.checkout_step_items_container}>
          {items.map(({id, price, image, alt, name, newQuantity}:CartItemType)=>
            <ItemCheckOut
            key={id}
            id={id}
            price={price}
            image={image}
            alt ={alt}
            name= {name}
            newQuantity={newQuantity}
          />)}
        </div>
    </div>
  )
}

export default Summary