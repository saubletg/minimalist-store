import React, { useState } from 'react'
import OrderSummary from '../OrderSummary/OrderSummary'
import PaymentForm from '../PaymentForm/PaymentForm'
import ShippingForm from '../ShippingForm/ShippingForm'
import Summary from '../Summary/Summary'
import styles from './CheckOutBody.module.scss'

function CheckOutBody() {
  const [shippingData, setShippingData] = useState({
    email: "",
    fullName: "",
    streetAddress1: "",
    streetAddress2: "",
    city: "",
    state: "",
    zip: "",
    country: ""
  });

  const [cardData, setCardData] = useState({
    cardNumber: "",
    expirationDate: "",
    securityCode: "",
  });

const handleShippingChange= (e:any) => {
  setShippingData({
      ...shippingData,
      [e.target.name] : e.target.value,
  })
}

const handleCardChange= (e:any) => {
  setCardData({
      ...cardData,
      [e.target.name] : e.target.value,
  })
}

  return (
    <div className={styles.checkout_body_container}>
        <Summary/>
        <ShippingForm 
        handleShippingChange={handleShippingChange}
        data= {shippingData}/>
        <PaymentForm
        handleCardChange= {handleCardChange}
        data= {cardData}/>
        <OrderSummary
        shippingData= {shippingData}
        cardData = {cardData}/>
    </div>
  )
}

export default CheckOutBody