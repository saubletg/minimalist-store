import React from 'react'
import Item from '../Item/Item'
import styles from './Items.module.scss'
import {data} from '../../data/data'
import { ItemType } from '../../typings';

function Items({itemsRef}:any) {
  const items = data.sneakers;
  return (
    <div className={styles.items_container} ref={itemsRef}>
        <h2 className={styles.items_h2}>
            Items
        </h2>
        <div className={styles.items_wrapper}>
            {items.map(({id, badge,price, image, alt, description, name, quantity}:ItemType,i:any)=>
            <Item
            key={i}
            id={id}
            badge={badge}
            price={price}
            image={image}
            alt ={alt}
            description={description}
            name= {name}
            quantity= {quantity}
            />)}
        </div>
    </div>
  )
}

export default Items