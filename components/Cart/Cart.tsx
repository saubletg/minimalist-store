import Link from 'next/link'
import React from 'react'
import {IoMdClose} from 'react-icons/io'
import { useSelector } from 'react-redux'
import { selectItems, selectTotal } from '../../slices/basketSlice'
import { CartItemType, ItemType } from '../../typings'
import CartItem from '../CartItem/CartItem'
import styles from './Cart.module.scss'

interface functionProps{
    handleCart(): void
}
const Cart : React.FC <functionProps>= ({handleCart}) => {
    const items = useSelector(selectItems)
    const total = useSelector(selectTotal)
    console.log(items)
  return (
    <div className={styles.cart_wrapper}>
        <div className={styles.icon_wrapper} onClick={handleCart}>
            <IoMdClose/>
        </div>
        <div className={styles.cart_title_wrapper} onClick={handleCart}>
            <h2 className={styles.cart_title}>
                Your Cart
            </h2>
        </div>
        {items.length === 0 ?
        <div className={styles.cart_items_wrapper} onClick={handleCart}>
            No items found.
        </div>
        :
        items.map(({id, price, image, alt, name, newQuantity}:CartItemType, i:any)=>
            <CartItem
            key={i}
            id={id}
            price = {price}
            image={image}
            alt ={alt}
            name= {name}
            newQuantity={newQuantity}
            />)
        }
        {items.length > 0 && <>
        <div className={styles.cart_subtotal_wrapper}>
            <div className={styles.cart_subtotal_title}>
                Subtotal
            </div>
            <div className={styles.cart_subtotal_price}>
                $ {total} USD
            </div>
        </div>
        <div className={styles.cart_button_wrapper}>
            <Link href="/checkout" >
                <a className={styles.item_buy}>Buy Now</a>
            </Link>
        </div>
        </>}
        
    </div>
  )
}

export default Cart