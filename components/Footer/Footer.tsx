import Link from 'next/link'
import React from 'react'
import styles from './Footer.module.scss'

function Footer() {
  return (
    <footer className={styles.footer_container}>
        <h2 className={styles.footer_title}>
            Minimalist Store
        </h2>
        <div className={styles.footer_copyright}>
            c Minimalist Store 2022
        </div>
        <div className={styles.footer_link_wrapper}>
            <div className={styles.footer_link}>
                <Link href="/">
                    <a href="">
                        Home
                    </a>
                </Link>
            </div>
            <div className={styles.footer_link}>
                <Link href="/">
                    <a href="">
                        Contact
                    </a>
                </Link>
            </div>
        </div>
    </footer>
  )
}

export default Footer