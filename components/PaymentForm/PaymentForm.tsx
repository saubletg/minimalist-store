import React, { useState } from 'react'
import styles from './PaymentForm.module.scss'

function PaymentForm({handleCardChange, data}:any) {
    

  return (
    <div className={styles.checkout_body_step_wrapper}>
        <div className={styles.checkout_step_title_wrapper}>
            <h2 className={styles.checkout_step_title}>
                3.Payment
            </h2>
        </div>
        <div className={styles.checkout_step_form_container}>
            <form action="" className={styles.shipping_form}>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="" className={styles.shipping_label}>Card Number.</label>
                    <input type="text" className={styles.shipping_input} name="cardNumber" 
                    onChange={handleCardChange} value={data.cardNumber}/>
                </fieldset>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="" className={styles.shipping_label}>Expiration Date .</label>
                    <input type="text" className={styles.shipping_input} name="expirationDate" 
                    onChange={handleCardChange} value={data.expirationDate}/>
                </fieldset>
                <fieldset className={styles.shipping_field}>
                    <label htmlFor="" className={styles.shipping_label}>Security Code .</label>
                    <input type="text" className={styles.shipping_input} name="securityCode" 
                    onChange={handleCardChange} value={data.securityCode}/>
                </fieldset>
            </form>
        </div>
    </div>
  )
}

export default PaymentForm