import React from 'react'
import styles from './ItemCheckOut.module.scss'
import Image from 'next/image'
import { CartItemType, ItemType } from '../../typings'

function ItemCheckOut({id, price, image, alt, name, newQuantity}:CartItemType) {
  return ( 
    <div className={styles.item_container}>
        <div className={styles.item_wrapper}>
            <div className={styles.item_image_wrapper}>
                <Image
                src={`/${image}`}
                alt={alt}
                className={styles.image}
                height="200"
                width="200"
                objectFit= 'contain'/>
            </div>
            <div className={styles.item_add_wrapper}>
                <h3 className={styles.item_title}>{name}</h3>
                <div className={styles.item_quantity_wrapper}>
                    Quantity : {newQuantity}
                </div>
            </div>
            <div className={styles.item_price_wrapper}>
                $ {price} USD
            </div>
        </div>
    </div>
  )
}

export default ItemCheckOut