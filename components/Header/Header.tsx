import Link from 'next/link'
import { useRouter } from "next/router";
import React, { useState } from 'react'
import styles from './Header.module.scss'
import {FaShoppingCart} from 'react-icons/fa'
import {CgMenuGridO} from 'react-icons/cg'
import Menu from '../Menu/Menu'
import Cart from '../Cart/Cart'
import {signIn, signOut, useSession} from 'next-auth/react'
import { useSelector } from 'react-redux';
import { selectItems } from '../../slices/basketSlice';


const Header: React.FC = () => {
    const {data: session, status}:any = useSession()
    const [showMenu, setShowMenu]= useState<boolean>(false);
    const [showCart, setShowCart]= useState<boolean>(false);
    const items = useSelector(selectItems)

    const handleMenu = () => {
        setShowMenu(!showMenu);
    };
    const handleCart = () => {
        setShowCart(!showCart);
    };

    const router = useRouter();


  return (
    <header className={styles.header_container}>
        <div className={styles.menu_wrapper} onClick={handleMenu}>
            <CgMenuGridO/>
        </div>
        <div className={styles.logo_wrapper}>
            <Link href="/">
                <a className={styles.logo}>
                    Minimalist Store
                </a>
            </Link>
        </div>
        
        <div className={styles.right_container}>
            <nav className={styles.header_nav}>
                <div className={router.pathname == "/" ? styles.link_wrapper_active : styles.link_wrapper}>
                    <Link href="/">
                        <a>
                            Home
                        </a>
                    </Link>
                </div>
                <div className={router.pathname == "/contact" ? styles.link_wrapper_active : styles.link_wrapper}>
                    <Link href="/contact">
                        <a>
                            Contact
                        </a>
                    </Link>
                </div>
                
            </nav>
            <div className={styles.cart_wrapper} onClick={handleCart}>
                <FaShoppingCart/>
                <div className={styles.counter}>
                    {items.reduce((total:any, item:any) => total + item.newQuantity, 0)}
                </div>
            </div>
        </div>
        {showMenu && 
        <Menu
        handleMenu = {handleMenu}/>
        }
        {showCart && 
        <Cart
        handleCart = {handleCart}/>
        }
    </header>
  )
}

export default Header