import Link from 'next/link'
import React from 'react'
import styles from './ContactForm.module.scss'

function ContactForm() {
    
  return (
    <div className={styles.contact_form_container}>
        <div className={styles.contact_form_wrapper}>
        <div className={styles.contact_form_title_wrapper}>
            <h2 className={styles.contact_form_title}>
                Contact
            </h2>
        </div>
        <div className={styles.contact_form_text_wrapper}>
            <p className={styles.contact_form_text}>
                Do you have questions or comments about our sneakers! Send us a message here and well get back to you as quickly as possible!
            </p>
        </div>
        <div className={styles.contact_form_container}>
            <form action="" className={styles.contact_form}>
                <fieldset className={styles.contact_field}>
                    <label htmlFor="email" className={styles.contact_label}>Name .</label>
                    <input type="email" className={styles.contact_input} name="email"/>
                </fieldset>
                <fieldset className={styles.contact_field}>
                    <label htmlFor="name" className={styles.contact_label}>Email .</label>
                    <input type="text" className={styles.contact_input} name="fullName"/>
                </fieldset>
                <fieldset className={styles.contact_field}>
                    <label htmlFor="address" className={styles.contact_label}>Message .</label>
                    <textarea className={styles.contact_area} name="message" maxLength={5000}/>
                </fieldset>
                <div className={styles.contact_button_wrapper}>
                    <Link href="/checkout" >
                        <a className={styles.contact_send}>Send Message</a>
                    </Link>
                </div>
            </form>
        </div>
        </div>
    </div>
  )
}

export default ContactForm