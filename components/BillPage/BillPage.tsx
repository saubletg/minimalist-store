import Link from 'next/link'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectItems, selectTotal } from '../../slices/basketSlice'
import { resetForm, selectValues } from '../../slices/formSlice'
import { resetBasket } from '../../slices/basketSlice'
import styles from './BillPage.module.scss'
import { CardType, CartItemType, InfosType } from '../../typings'

function BillPage() {
    const items = useSelector(selectItems)
    const total = useSelector(selectTotal)
    const values = useSelector(selectValues)

    const dispatch= useDispatch();
    const resetAllBasket = () => {
        dispatch(resetBasket())
        dispatch(resetForm())
    }
    if (values.length > 0) {
  return (
    <>
        <div className={styles.bill_head_container}>
            <div className={styles.bill_title_wrapper}>
                <h1 className={styles.bill_title}>Thank You !</h1>
                <div className={styles.bill_title_line_wrapper}>
                    <div className={styles.bill_title_line}></div>
                </div>
            </div>
        </div>
        <div className={styles.bill_container}>
            <div className={styles.bill_order_container}>
                <div className={styles.bill_order_title_wrapper}>
                    <h2 className={styles.bill_order_title}>
                        Order Summary
                    </h2>
                </div>
                <div className={styles.bill_recap_wrapper}>
                    {items.map(({id, price, name, newQuantity}:CartItemType)=>
                    <div className={styles.bill_item_wrapper} key={id}>
                        <div className={styles.bill_recap_name}>
                            .{name}
                        </div>
                        <div className={styles.bill_recap_price}>
                            $ {price} USD
                        </div>
                        <div className={styles.bill_recap_quantity}>
                            x{newQuantity}
                        </div>
                    </div>)
                    }
                </div>
                <div className={styles.bill_total_wrapper}>
                    <div className={styles.bill_total}>
                        Total
                    </div>
                    <div className={styles.bill_total_price}>
                        $ {total} USD
                    </div>
                </div>
            </div>
            <div className={styles.bill_shipping_container}>
                <div className={styles.bill_shipping_title_wrapper}>
                    <h2 className={styles.bill_shipping_title}>
                        Shipping Summary
                    </h2>
                </div>
                <div className={styles.bill_shipping_wrapper}>
                    {values[0].filter((e: any)=> e.email).map(({
                        email,
                        fullName,
                        streetAddress1,
                        streetAddress2,
                        city,
                        state, 
                        zip, 
                        country}:InfosType, i:any)=>
                    <div className={styles.bill_values_wrapper} key={i}>
                        <div className={styles.bill_value}>
                            Email : {email}
                        </div>
                        <div className={styles.bill_value}>
                            Fullname: {fullName}
                        </div>
                        <div className={styles.bill_value}>
                        Street Address : {streetAddress1}, {streetAddress2}
                        </div>
                        <div className={styles.bill_value}>
                        City : {city}
                        </div>
                        <div className={styles.bill_value}>
                        State/Province : {state}
                        </div>
                        <div className={styles.bill_value}>
                        Zip/Postal Code : {zip}
                        </div>
                        <div className={styles.bill_value}>
                        Country : {country}
                        </div>
                    </div>)
                    }
                </div>
            </div>
            <div className={styles.bill_shipping_container}>
                <div className={styles.bill_shipping_title_wrapper}>
                    <h2 className={styles.bill_shipping_title}>
                        Card Summary
                    </h2>
                </div>
                <div className={styles.bill_shipping_wrapper}>
                    {values[0].filter((e: any)=> e.cardNumber).map(({cardNumber, expirationDate, securityCode}:CardType, i:any)=>
                    <div className={styles.bill_values_wrapper} key={i}>
                        <div className={styles.bill_value}>
                        Card Number : {cardNumber}
                        </div>
                        <div className={styles.bill_value}>
                        Expiration Date: {expirationDate}
                        </div>
                        <div className={styles.bill_value}>
                        Security Code : {securityCode}
                        </div>
                    </div>)
                    }
                </div>
            </div>
            <div className={styles.bill_button_wrapper}>
                <Link href="/">
                <a className={styles.bill_button} onClick={resetAllBasket}>
                    Back to the Store !
                </a>
                </Link>
            </div>
        </div>
    </>
  )}
  return null
}

export default BillPage