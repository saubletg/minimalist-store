import React from 'react'
import styles from './CartItem.module.scss'
import Image from 'next/image'
import { removeFromBasket } from '../../slices/basketSlice'
import { useDispatch } from 'react-redux'
import { CartItemType, ItemType } from '../../typings'

function CartItem({id, price, image, alt, name, newQuantity}:CartItemType) {
    const dispatch= useDispatch();
    const removeItemFromBasket = () => {
        dispatch(removeFromBasket({id}))
    }
  return ( 
    <div className={styles.item_container}>
        <div className={styles.item_wrapper}>
            <div className={styles.item_image_wrapper}>
                <Image
                src={`/${image}`}
                alt={alt}
                className={styles.image}
                height="200"
                width="200"
                objectFit= 'contain'/>
            </div>
            <div className={styles.item_add_wrapper}>
                <h3 className={styles.item_title}>{name}</h3>
                <div className={styles.item_quantity_wrapper}>
                $ {price} USD
                </div>
                <button className={styles.item_button_remove} onClick={removeItemFromBasket}>
                    Remove Item
                </button>
            </div>
            <div className={styles.item_price_wrapper}>
                {newQuantity}
            </div>
        </div>
    </div>
  )
}

export default CartItem