export const data =  {
    sneakers: [
      {
        name: "Model 01",
        id: 101,
        badge: true,
        price: 400,
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus placeat culpa quibusdam necessitatibus, ut odit facere earum tenetur, quis velit ducimus fugit laborum voluptas deserunt nemo soluta aperiam obcaecati architecto voluptate itaque consequuntur amet laboriosam cum ratione! Obcaecati, quos nam.",
        image: "model01.png",
        alt: "Model 01",
        quantity: 1
      },
      {
        name: "Model 02",
        id: 201,
        badge: true,
        price: 250,
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus placeat culpa quibusdam necessitatibus, ut odit facere earum tenetur, quis velit ducimus fugit laborum voluptas deserunt nemo soluta aperiam obcaecati architecto voluptate itaque consequuntur amet laboriosam cum ratione! Obcaecati, quos nam.",
        image: "model02.png",
        alt: "Model 02",
        quantity: 1
      },
      {
        name: "Model 03",
        badge: false,
        id: 301,
        price: 500,
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus placeat culpa quibusdam necessitatibus, ut odit facere earum tenetur, quis velit ducimus fugit laborum voluptas deserunt nemo soluta aperiam obcaecati architecto voluptate itaque consequuntur amet laboriosam cum ratione! Obcaecati, quos nam.",
        image: "model03.png",
        alt: "Model 04",
        quantity: 1
      },
      {
        name: "Model 04",
        badge: true,
        id: 401,
        price: 350,
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus placeat culpa quibusdam necessitatibus, ut odit facere earum tenetur, quis velit ducimus fugit laborum voluptas deserunt nemo soluta aperiam obcaecati architecto voluptate itaque consequuntur amet laboriosam cum ratione! Obcaecati, quos nam.",
        image: "model04.png",
        alt: "Model 04",
        quantity: 1
      },
      {
        name: "Model 05",
        badge: true,
        id: 501,
        price: 275,
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus placeat culpa quibusdam necessitatibus, ut odit facere earum tenetur, quis velit ducimus fugit laborum voluptas deserunt nemo soluta aperiam obcaecati architecto voluptate itaque consequuntur amet laboriosam cum ratione! Obcaecati, quos nam.",
        image: "model05.png",
        alt: "Model 05",
        quantity: 1
      },
      {
        name: "Model 06",
        badge: false,
        id: 601,
        price: 300,
        description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus placeat culpa quibusdam necessitatibus, ut odit facere earum tenetur, quis velit ducimus fugit laborum voluptas deserunt nemo soluta aperiam obcaecati architecto voluptate itaque consequuntur amet laboriosam cum ratione! Obcaecati, quos nam.",
        image: "model06.png",
        alt: "Model 06",
        quantity: 1
      },
    ]
}