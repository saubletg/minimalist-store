import {configureStore} from "@reduxjs/toolkit";
import basketReducer from "../slices/basketSlice";
import formReducer from "../slices/formSlice";

//global store setup
export const store = configureStore({
    reducer: {
        basket: basketReducer,
        form: formReducer
    },
});