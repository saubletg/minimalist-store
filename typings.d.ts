export interface ItemType {
    id: number; 
    badge: boolean; 
    price: number; 
    image: string; 
    alt: string; 
    description: string; 
    name: string; 
    quantity: number;
}

export interface CartItemType {
    id: number; 
    price: number; 
    image: string; 
    alt: string; 
    name: string; 
    newQuantity: number;
}

export interface InfosType {
    email: string;
    fullName: string;
    streetAddress1: string;
    streetAddress2: string;
    city:string;
    state: string; 
    zip: string; 
    country: string;
}

export interface CardType {
    cardNumber: number; 
    expirationDate: number; 
    securityCode: number;
}



